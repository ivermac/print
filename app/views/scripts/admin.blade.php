<script type="text/javascript">
	$(function(){
		/*global variables*/
		var same_text, change_status, url;

		/*prevent event if the text of the status button is same as the text of the option selected*/
		$( "a[class^='status']" ).
			on("click", function(){
				if ( same_text($(this)) ) {
					return false;
				}
				var status, id, selected;
				selected = $(this);
				status = selected.text();
				id = selected.attr('class').split("-")[1];
				change_status(status, id);
			}).
			hover(function(){
				$(this).removeAttr('style');
				if ( same_text($(this)) ) {
					$(this).css({'cursor': 'not-allowed'});
				}
			});

		/*check if the text of the status button is the same as the text of the option selected*/
		same_text = function(object){
			return object.text().trim() === object.parent().parent().parent().find('.current-status').text().trim();
		}

		/*change the status of the transaction to either 'PENDING', 'IN PROGRESS', 'DONE' and 'DELIVERING'*/
		change_status = function(status, id){
			var selector;
			url = "{{ URL::to('home/changestatus/') }}"
			selector = "button[dropdown-id='"+id+"']";
			var initial_status = $(selector).text().trim();
			var caret = $('<span>').attr({'class':'caret'});
			$.ajax({
				type: "POST",
				url:url,
				data: {
					"id": id,
					"status": status,
				},
				beforeSend: function(){
					$(selector).text('LOADING... ').append(caret);
				},
				success: function(data) {
					console.log("response >>> "+data);
					$(selector).text(status+" ").append(caret);
				},
				error: function(xhr, text, error) {
					$(selector).text(initial_status+" ").append(caret);
					console.log("xhr >>> "+xhr);
					console.log("text >>> "+text);
					console.log("error >>> "+error);
				},
			});
		}

		/*pop-up dialog with details of a specific transaction*/
		$('.view-details').on("click", function(){
			var link, id;
			link = $(this);
			url = "{{ URL::to('home/portfoliodetail/') }}"
			id = link.attr('portfolio-id');
			console.log("id >>> "+id);
			$.ajax({
				type: "POST",
				url:url,
				data: {
					"id": id,
				},
				beforeSend: function(){
					$('#dd-file-name').empty().append('loading...')
					$('#dd-description').empty().append('loading...')
					$('#dd-status').empty().append('loading...')
				},
				success: function(data) {
					$('#dd-file-name').empty().append(data.name)
					$('#dd-description').empty().append(data.description)
					$('#dd-status').empty().append(data.status)
				},
				error: function(xhr, text, error) {
					console.log("xhr >>> "+xhr);
					console.log("text >>> "+text);
					console.log("error >>> "+error);
				},
			});
		});
	})
</script>