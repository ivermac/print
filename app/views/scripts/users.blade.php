<script type="text/javascript">
	$(function(){
		$('.view-details').on("click", function(){
			var link, url, id;
			link = $(this);
			url = "{{ URL::to('home/portfoliodetail/') }}"
			id = link.attr('portfolio-id');
			console.log("id >>> "+id);
			$.ajax({
				type: "POST",
				url:url,
				data: {
					"id": id,
				},
				beforeSend: function(){
					$('#dd-file-name').empty().append('loading...')
					$('#dd-description').empty().append('loading...')
					$('#dd-status').empty().append('loading...')
				},
				success: function(data) {
					$('#dd-file-name').empty().append(data.name)
					$('#dd-description').empty().append(data.description)
					$('#dd-status').empty().append(data.status)
				},
				error: function(xhr, text, error) {
					console.log("xhr >>> "+xhr);
					console.log("text >>> "+text);
					console.log("error >>> "+error);
				},
			});
		});

		$('#help-guide').on('click', function(){
			guiders.createGuider({
			  buttons: [{name: "Close"}, {name: "Next"}],
			  description: "Guiders are a user interface design pattern for introducing features of software. This dialog box, for example, is the first in a series of guiders that together make up a guide.",
			  id: "first",
			  next: "second",
			  overlay: true,
			  title: "Welcome to Print and Allied user guide"
			}).show();
		})
	});
</script>