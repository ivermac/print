
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Print and Allied</title>

	{{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
    {{ HTML::style('css/cover.css') }}
    {{ HTML::script('js/jquery-1.9.1.min.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Print and Allied</h3>
              <ul class="nav masthead-nav">
                <li class="active">{{ HTML::link('/', 'Home') }}</li>
                <li>{{ HTML::link('users/register', 'Users') }}</li>
                <li>{{ HTML::link('admin/login', 'Admin') }}</li>
              </ul>
            </div>
          </div>

          <div class="inner cover">
            <h1 class="cover-heading">Print and Allied</h1>
            <p class="lead">Your trusted printing service</p>
            <p class="lead">
            	{{ HTML::link('users/register', 'Register with us', array('class' => 'btn btn-lg btn-default')) }}
            </p>
          </div>

          <div class="mastfoot">
            <div class="inner">
            </div>
          </div>

        </div>

      </div>

    </div>

  </body>
</html>
