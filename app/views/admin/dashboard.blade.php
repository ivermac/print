<h1 class="col-md-8 col-md-offset-4">Admin Dashboard</h1>

<div class="row">
	<div class="col-md-12">
		<div class="row">
			@if(count($portfolios)>0)
				<table class="table table-striped">
					<thead>
						<tr>
							<td>Type</td>
							<td>File name</td>
							<td>Email</td>
							<td>Description</td>
							<td>Type</td>
							<td>Status</td>
							<td></td>
						</tr>
					</thead>
				@foreach ($portfolios as $id => $portfolio)
					<tr>
						<td>{{ HTML::image('extension-images/'.$portfolio["icon"], '...') }}</td>
						<td>{{ $portfolio['name'] }}</td>
						<td>{{ $portfolio['email'] }}</td>
						<td>{{ (strlen($portfolio['description']) > 50) ? substr($portfolio['description'], 0, 50)."..." : $portfolio['description'] }}</td>
						<td>{{ $portfolio['type'] }}</td>
						<td>
							<div class="btn-group">
							  <button type="button" class="btn btn-default dropdown-toggle current-status" data-toggle="dropdown" dropdown-id="{{ $portfolio['id'] }}">
							    {{ $portfolio['status'] }} <span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu status" role="menu" data-id="{{ $portfolio['id'] }}">
							    <li><a href="#" class="status-{{ $portfolio['id'] }}">PENDING</a></li>
							    <li><a href="#" class="status-{{ $portfolio['id'] }}">IN PROGRESS</a></li>
							    <li><a href="#" class="status-{{ $portfolio['id'] }}">DONE</a></li>
							    <li><a href="#" class="status-{{ $portfolio['id'] }}">DELIVERING</a></li>
							  </ul>
							</div>
						</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown">
							    	Action <span class="caret"></span>
							  	</button>

							  	<ul class="dropdown-menu" role="menu">
							    	<li>{{ HTML::link($portfolio['download_link'],"Download", array("class"=>"")) }}</li>
							    	<li class="divider"></li>
							    	<li>{{ HTML::link('#', 'View details', array('data-toggle'=>'modal' ,'data-target'=>'#detail-modal', 'portfolio-id'=>$portfolio['id'], 'class'=>'view-details')) }}</li>
							  	</ul>
							</div>
						</td>
					</tr>	
				@endforeach
				</table>
			@else
				data not available :(
			@endif
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="detail-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Portfolio details</h4>
      </div>
      <div class="modal-body">
        <dl class="dl-horizontal">
          <dt>File name</dt>
          <dd id="dd-file-name"></dd>
          <dt>Description</dt>
          <dd id='dd-description'></dd>
          <dt>Status</dt>
          <dd id='dd-status'></dd>
        </dl>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-custom" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->