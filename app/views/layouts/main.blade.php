<!DOCTYPE html>
<html lang="en">
  	<head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    	<title>Print and Allied</title>
    	{{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('css/main.css')}}
        {{ HTML::style('css/guiders-1.2.8.css')}}

        {{ HTML::script('js/jquery-1.9.1.min.js') }}
        {{ HTML::script('js/guiders-1.2.8.js')}}
        {{ HTML::script('js/bootstrap.min.js') }}
        {{ $custom_script }}
  </head>
 
  <body>
 	<div class="navbar navbar-fixed-top navbar-inverse">
       	<div class="container">
          	<div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                 	<ul class="nav navbar-nav pull-right">  
                    	@if(!Auth::check())
                          <li>{{ HTML::link('/', 'Home') }}</li>   
        	               <li>{{ HTML::link('users/register', 'Register') }}</li>   
        	               <li>{{ HTML::link('users/login', 'Login') }}</li>   
        	            @else
                         <li>
                            {{ $profile_menu_item }}
                         </li>
                         <li>
                            {{ HTML::link('#', 'Help', array('id'=>'help-guide')) }}
                         </li>
        	            @endif  
                 	</ul> 
                </div> 
          	</div>
       	</div>
    </div>

    <div class="container">
      	@if(Session::has('message'))
         	<p class="alert {{ (Session::has('custom-alert')) ? Session::get('custom-alert') : 'alert-warning' }} .alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('message') }}
            </p>
      	@endif
    	{{ $content }}
    </div>

  </body>
</html>