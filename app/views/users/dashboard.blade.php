<div class="row">
	<h1 class="col-md-4 col-md-offset-4">Dashboard</h1>
	<hr>
</div>
<div class="row">
	<div class="col-md-3 portfolio-form">
		<h3>Add Portfolio</h3>
		{{ Form::open(array('url' => 'users/portfolio', 'files' => true, 'method' => 'post')) }}
			<div class="form-group">
				{{ Form::label('portfolio-label', 'Portfolio', array('class' => 'app-labels')) }}
				{{ Form::file('portfolio', $attributes = array('class' => 'form-control')) }}
		  	</div>
		  	<div class="form-group">
				{{ Form::label('type-label', 'Type', array('class' => 'app-labels')) }}<br>
				{{ 
					Form::select('type', 
						$types,
						'',
						array(
							'class' => 'form-control',
						)
					) 
				}}
		  	</div>
		  	<div class="form-group">
				{{ Form::label('desired-outcome-label', 'Desired outcome description', array('class' => 'app-labels')) }}
				{{ Form::textarea('description', '', array('cols' => '40', 'rows' => '6', 'class' => 'description form-control')) }}
		  	</div>
		{{ Form::submit('Submit portfolio', array('class' => 'btn btn-primary btn-custom file-submit')) }}
		{{ Form::close() }}
	</div>
	<div class="col-md-8 data">
		
		<div class="row">
			@if(!empty($paginator))
				<table class="table">
					<thead>
						<tr>
							<td>Type</td>
							<td>File name</td>
							<td>Description</td>
							<td>Type</td>
							<td>Status</td>
							<td></td>
						</tr>
					</thead>
				@foreach ($paginator as $id => $portfolio)
					<tr>
						<td>{{ HTML::image('extension-images/'.$portfolio["icon"], '...') }}</td>
						<td>{{ $portfolio['name'] }}</td>
						<td>{{ (strlen($portfolio['description']) > 30) ? substr($portfolio['description'], 0, 40)."..." : $portfolio['description'] }}</td>
						<td>{{ $portfolio['type'] }}</td>
						<td>{{ $portfolio['status'] }}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown">
							    	Action <span class="caret"></span>
							  	</button>
							  	<ul class="dropdown-menu" role="menu">
							    	<li>{{ HTML::link($portfolio['download_link'],"Download", array("class"=>"")) }}</li>
							    	<li class="divider"></li>
							    	<li>{{ 
							    			HTML::link('#', 'View details', 
							    				array(
							    					'data-toggle'=>'modal' ,
							    					'data-target'=>'#detail-modal', 
							    					'portfolio-id'=>$portfolio['id'], 
							    					'class'=>'view-details'
							    					)
							    				) 
							    			}}
							    	</li>
							    	<li class="divider"></li>
							    	<li>{{ HTML::link($portfolio['delete_link'],"Delete", array("class"=>"")) }}</li>
							  	</ul>
							</div>
						</td>
					</tr>	
				@endforeach
				</table>
				{{ $paginator->links(); }}
			@else
				<div class="alert alert-warning">
					data not available :(
				</div>
			@endif
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="detail-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Portfolio details</h4>
      </div>
      <div class="modal-body">
        <dl class="dl-horizontal">
          <dt>File name</dt>
          <dd id="dd-file-name"></dd>
          <dt>Description</dt>
          <dd id='dd-description'></dd>
          <dt>Status</dt>
          <dd id='dd-status'></dd>
        </dl>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-custom" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->