<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        {{ HTML::decode('<span class="glyphicon glyphicon-user"></span> '.Auth::user()->firstname." ".Auth::user()->lastname) }} 
        <b class="caret"></b>
    </a>
        
    <ul class="dropdown-menu">
        <li>
            <p class="profile-details">{{ '<span class="glyphicon glyphicon-envelope"></span> '.Auth::user()->email }}</p>
        </li>
        <li>
            <p class="profile-details">{{ '<span class="glyphicon glyphicon-earphone"></span> '.Auth::user()->phonenum }}</p>
        </li>
        <li>
            <p class="profile-details">{{ '<span class="glyphicon glyphicon-tower"></span> '.Auth::user()->organisation }}</p>
        </li>
        <li class="divider"></li>
        <li>{{ HTML::decode(HTML::link('users/logout', '<span class="glyphicon glyphicon-log-out"></span> logout')) }}</li>
    </ul>
</li>