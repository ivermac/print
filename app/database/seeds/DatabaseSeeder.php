<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');

		$this->call('UserTableSeeder');
        $this->command->info('Users table seeded!');

        $this->call('AdminTableSeeder');
        $this->command->info('Admins table seeded!');

        $this->call('PortfolioTableSeeder');
        $this->command->info('Portfolios table seeded!');
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(
        	array(
        		'firstname' => 'mark',
        		'lastname' => 'ekisa',
        		'phonenum' => '0722450790',
        		'organisation' => 'ajabworld',
        		'email' => 'mark.ekisa@gmail.com',
        		'password' => Hash::make('12345678'),
        	)
        );
    }

}

class AdminTableSeeder extends Seeder {

    public function run()
    {
        DB::table('admins')->delete();

        Admin::create(
        	array(
        		'firstname' => 'mark',
        		'lastname' => 'ekisa',
        		'email' => 'mark.ekisa@gmail.com',
        		'password' => Hash::make('12345678'),
        	)
        );
    }

}

class PortfolioTableSeeder extends Seeder {

    public function run()
    {
        DB::table('portfolios')->delete();

        Portfolio::create(
        	array(
        		'email' => 'mark.ekisa@gmail.com',
        		'name' => '404.png',
        		'type' => 'resume',
        		'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
        						  labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
        						  laboris nisi ut aliquip ex ea commodo consequat.',
        		'status' => 'PENDING',
        	)
        );

        Portfolio::create(
        	array(
        		'email' => 'mark.ekisa@gmail.com',
        		'name' => '2.pdf',
        		'type' => 'poster',
        		'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
        						  labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
        						  laboris nisi ut aliquip ex ea commodo consequat.',
        		'status' => 'PENDING',
        	)
        );
    }

}