<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('main');
	}

	public function getDownload($file_name, $id = null){
		if ($id != null ){
			$portfolio = Portfolio::where('id', $id)->first();
			$email = $portfolio->email;
		} else {
			$email = Auth::user()->email;
		}
		$path = 'portfolios/'.md5($email);	

		$size = filesize($path."/".$file_name);
		header("Content-Type: application/force-download; name=\"". $file_name ."\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ". $size ."");
		header("Content-Disposition: attachment; filename=\"". $file_name ."\"");
		header("Expires: 0");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		echo (readfile($path."/".$file_name));
	}

	public function postChangestatus(){
		$id = Input::get('id');
		$status = Input::get('status');
		$portfolio = Portfolio::where('id', $id)->first();
		$portfolio->status = $status;
		$portfolio->save();

		return "Status updated!!!";
	}

	public function postPortfoliodetail(){
		$id = Input::get('id');
		$portfolio = Portfolio::where('id', $id)->first();
		return  $portfolio->toArray();
	}

	public function getEmail(){
		Mail::send('emails.welcome', $data = array(), function($message)
		{
			$message->from('info@ivermac.com', 'ivermac');
		    $message->to('mark.ekisa@gmail.com', 'Mark Ekisa')->subject('Welcome!');
		});
	}

}