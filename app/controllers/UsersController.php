<?php
 
class UsersController extends BaseController {
	protected $layout = "layouts.main";

	public function __construct() {
	   $this->beforeFilter('csrf', array('on'=>'post'));
	   $this->beforeFilter('auth', array('only'=>array('getDashboard')));
	}

	public function getRegister() {
	   $this->layout->content = View::make('users.register');
	   $this->layout->custom_script = "";
	}

	public function postCreate() {
		$validator = Validator::make(Input::all(), User::$rules);
		 
	   	if ($validator->passes()) {
	   		$email = Input::get('email');
	   		$name = Input::get('firstname')." ".Input::get('lastname');

	    	$user = new User;
    	   	$user->firstname = Input::get('firstname');
    	   	$user->lastname = Input::get('lastname');
    	   	$user->phonenum = Input::get('phonenum');
    	   	$user->organisation = Input::get('organisation');
    	   	$user->email = $email;
    	   	$user->password = Hash::make(Input::get('password'));
    	   	$user->save();
    	 
	 	   	Mail::send('emails.welcome', $data = array(), function($message) use ($email, $name)
			{
				$message->from('info@ivermac.com', 'ivermac');
			    $message->to($email, $name)->subject('Welcome!');
			});
    	 	
    	   	return Redirect::to('users/login')->with('message', 'Thanks for registering!');
	   	} else {
	      	return Redirect::to('users/register')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();   
	   	}
	}

	public function getLogin() {
	   	$this->layout->content = View::make('users.login');
	   	$this->layout->custom_script = "";
	}

	public function getLogout() {
	   	Auth::logout();
	   	return Redirect::to('users/login')->with('message', 'Your are now logged out!');
	}

	public function postSignin() {
		if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
		   return Redirect::to('users/dashboard')->with('message', 'You are now logged in!');
		} else {
		   return Redirect::to('users/login')
		      ->with('message', 'Your username/password combination was incorrect')
		      ->withInput();
		}         
	}

	public function postPortfolio(){
		$file = Input::file('portfolio');
		$description = Input::get('description');
		$path = 'portfolios/'.md5(Auth::user()->email);

		$validator = Validator::make(Input::all(), Portfolio::rules());
		if ($validator->fails())
		{
			return Redirect::to('users/dashboard')
					->with('message', 'Both the file and description fields are required, kindly add them before submission')
					->with('custom-alert', 'alert-danger')
					->withInput();
		}

		$portfolio = new Portfolio;
		$portfolio->email = Auth::user()->email;
		$portfolio->name = $file->getClientOriginalName();
		$portfolio->description = Input::get('description');
		$portfolio->type = Input::get('type');
		$portfolio->status = "PENDING";
		$portfolio->save();
		Log::info('Details of new portfolio added', array('portfolio' => $portfolio));

		Input::file('portfolio')->move($path, $file->getClientOriginalName());
		Log::info('Portfolio added in directory', array('file' => $file));
		
		return Redirect::to('users/dashboard')
				->with('message', 'Portfolio has successfully been added');
	}

	public function getDashboard() {
		$email = Auth::user()->email;

		$portfolios = Portfolio::where('email', $email)->get();
		$paginator = 0;
		$types = array(
			'resume' => 'resume',
			'poster' => 'poster',
			'presetntation' => 'presetntation',
			'brochure' => 'brochure',
			'invitation' => 'invitation',
		);

		if (count($portfolios) > 0) {
			foreach ($portfolios as $portfolio) {
				$name = $portfolio->name;
				$id = $portfolio->id;
				$row['id'] = $id;
				$row['name'] = $name;
				$row['type'] = $portfolio->type;
				$row['description'] = $portfolio->description;
				$row['status'] = $portfolio->status;

				$icon = substr(strrchr($name,'.'),1).".png";
				$row['icon'] = $icon;
				$row['download_link'] = "/home/download/".$name;
				$row['delete_link'] = "/users/delete/".$id;

				$data[$portfolio->id] = $row;
			}
			$total_portfolios = count($data);
			$total_pages = ceil($total_portfolios / 5);
			$page = Input::get('page', 1);
			if ($page > $total_pages or $page < 1) $page = 1;
			$offset = ($page * 5) - 5;
			$data = array_slice($data, $offset, 5);
			$paginator = Paginator::make($data, $total_portfolios, 5);
		} 
		$this->layout->content = View::make('users.dashboard', array( 'paginator' => $paginator, 'types' => $types, 'user_name' => Auth::user()->firstname));
		$this->layout->custom_script = View::make('scripts.users');
		$this->layout->profile_menu_item = View::make('users.profile_menu_item');
	}

	public function getDelete($id){
		$portfolio = Portfolio::find($id);
		$path = 'portfolios/'.md5(Auth::user()->email).'/'.$portfolio->name;
		File::delete($path);
		$portfolio->delete();
		$message = "Record has successfully been deleted";
		return Redirect::to('users/dashboard')
				->with('message', $message);
	}

}
?>