<?php

class AdminController extends BaseController {
	protected $layout = "layouts.admin";

	public function __construct() {
	   	$this->beforeFilter('csrf', array('on'=>'post'));
	   	$this->beforeFilter('auth', array('only'=>array('getDashboard')));
	}

	public function getLogin() {
		$this->layout->content = View::make('admin.login');
		$this->layout->custom_script = "";
	}

	public function getRegister() {
	   	$this->layout->content = View::make('admin.register');
		$this->layout->custom_script = "";
	}

	public function postSignin() {
		if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
		   return Redirect::to('admin/dashboard')->with('message', 'You are now logged in!');
		} else {
		   return Redirect::to('admin/login')
		      ->with('message', 'Your username/password combination was incorrect')
		      ->withInput();
		}         
	}

	public function getLogout() {
	   	Auth::logout();
	   	return Redirect::to('admin/login')->with('message', 'Your are now logged out!');
	}

	public function postCreate() {
		$validator = Validator::make(Input::all(), Admin::rules());
		 
	   	if ($validator->passes()) {
	    	$admin = new Admin;
    	   	$admin->firstname = Input::get('firstname');
    	   	$admin->lastname = Input::get('lastname');
    	   	$admin->email = Input::get('email');
    	   	$admin->password = Hash::make(Input::get('password'));
    	   	$admin->save();
    	 
    	   	return Redirect::to('admin/login')->with('message', 'Thanks for registering!');
	   	} else {
	      	return Redirect::to('admin/register')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();   
	   	}
	}

	public function getDashboard() {
		$email = Auth::user()->email;
		$path = 'portfolios/'.md5($email);	
		$files = File::files($path);

		foreach ($files as $file) {
			$splitter = explode("/", $file);
			$file_name = $splitter[2];
			$images[$file] = array('ext' => File::extension($file).".png", 'name'=> $file_name) ;
		}	

		$portfolios = Portfolio::all();
		$data = array();
		if (count($portfolios) > 0) {
			foreach ($portfolios as $portfolio) {
				$name = $portfolio->name;
				$id = $portfolio->id;
				$row['id'] = $id;
				$row['name'] = $name;
				$row['description'] = $portfolio->description;
				$row['status'] = $portfolio->status;
				$row['type'] = $portfolio->type;
				$row['email'] = $portfolio->email;

				$icon = substr(strrchr($name,'.'),1).".png";
				$row['icon'] = $icon;
				$row['download_link'] = "/home/download/$name/$id";

				$data[$portfolio->id] = $row;
			}
		} 
		$this->layout->content = View::make('admin.dashboard', array( 'portfolios' => $data));
		$this->layout->custom_script = View::make('scripts.admin');
	}
}