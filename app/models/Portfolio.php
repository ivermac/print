<?php

class Portfolio extends Eloquent  {
	/**
	 * set the validation rules.
	 * @return array
	 */
	
	public static function rules() {
		return array(
			'portfolio' => 'required',
			'description' => 'required',
		);
	}
}