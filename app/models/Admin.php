<?php

class Admin extends Eloquent  {

	/**
	 * set the validation rules.
	 * @return array
	 */
	
	public static function rules() {
		return array(
		   'firstname'=>'required|alpha|min:2',
		   'lastname'=>'required|alpha|min:2',
		   'email'=>'required|email|unique:admins',
		   'password'=>'required|between:6,12|confirmed',
		   'password_confirmation'=>'required|between:6,12'
	   );
	}
}